var markerA = [
    ["世纪大道88号 200120",31.2364349,121.505852,1],
    ["银城中路501号 200120",31.233518,121.505522,2],
    ["延安东路222号 200002",31.2327659,121.4872509,3],
    ["世纪大道1229号 200122",31.229606,121.524184,4],
    ["大连路588号 200082",31.258596,121.512952,5],
    ["西藏中路168号 200001",31.2313165,121.477721,6],
    ["西藏中路268号 200001",31.23303,121.4767,7],
    ["黄河路21号 200003",31.233725,121.471172,8],
    ["北京西路968号 200041",31.230858,121.456432,9],
    ["南京西路1266号 200040",31.22744,121.4541,10],
    ["淮海中路999号 200031",31.21587,121.4576,11]];
        
var mapLoaded = false;
//Baidu map home
function createMap() {
	var map = new BMap.Map("near-map"); //Create a map in Baidu map container
	var point = new BMap.Point(114.3552290,30.5256530); //The definition of a center point coordinates (changes here)
	map.centerAndZoom(point, 6); //Center point and coordinate map set and the map is displayed on the map in the container
	window.map = map; //The map variable is stored in the global
}

function setMapEvent() {
	map.enableDragging(); //Enable map drag events, enabled by default (not write)
	map.enableScrollWheelZoom(); //Enable map wheel zoom
	map.enableDoubleClickZoom(); //Enabling the mouse to double-click to enlarge, enabled by default (not write)
	map.enableKeyboard(); //Enable keyboard arrow keys move the map
}

 function addMapControl() {
	//Add controls to a map of China "
	var ctrl_nav = new BMap.NavigationControl({ anchor: BMAP_ANCHOR_TOP_LEFT, type: BMAP_NAVIGATION_CONTROL_LARGE });
	map.addControl(ctrl_nav);
   
	//Add scale control to the map
	var ctrl_sca = new BMap.ScaleControl({ anchor: BMAP_ANCHOR_BOTTOM_LEFT });
	map.addControl(ctrl_sca);
}

function addMarker() {
    for (var i = 0; i <markerA.length; i++) {
		
      var json = markerA[i];
	  
	  var content = 'Title Here';
	  
	  json['title']='';
	  json['icon']='{}';
	  json['content']=json[0];
	  
      var p0 = json[2];
      var p1 = json[1];
      var point = new BMap.Point(p0, p1);
      var iconImg = createIcon(json.icon);
      var marker = new BMap.Marker(point, { icon: iconImg });
      var iw = createInfoWindow(i);
      var label = new BMap.Label(json.title, { "offset": new BMap.Size(json.icon.lb - json.icon.x + 10, -20) });
      marker.setLabel(label);
      map.addOverlay(marker);
      label.setStyle({
        borderColor: "#808080",
        color: "#333",
        cursor: "pointer"
      });

      (function () {
        var index = i;
        var _iw = createInfoWindow(i);
        var _marker = marker;
        _marker.addEventListener("click", function () {
          this.openInfoWindow(_iw);
        });
        _iw.addEventListener("open", function () {
          _marker.getLabel().hide();
        })
        _iw.addEventListener("close", function () {
          _marker.getLabel().show();
        })
        label.addEventListener("click", function () {
          _marker.openInfoWindow(_iw);
        })
        if (!!json.isOpen) {
          label.hide();
          _marker.openInfoWindow(_iw);
        }
      })()
    }
  }
  
  function createInfoWindow(i) {
    var json = markerA[i];
    var iw = new BMap.InfoWindow("<b class='iw_poi_title' title='" + json.title + "'>" + json.title + "</b><div class='iw_poi_content'>" + json.content + "</div>");
	return iw;
  }
  
  //Create a Icon
  function createIcon(json) {
    var icon = new BMap.Icon('https://www.easyoffices.com/images/rovva/index/marker.png', new BMap.Size(28,40))
    return icon;
  }

function baiduIni(){
	createMap();
	setMapEvent();
	addMapControl();
	addMarker();
}

function loadBaiduMapsAPI() {
	addScript('//api.map.baidu.com/api?v=2.0&ak=u6U9GSVtR213nYDo3DN43tfkpzYD6qn9&callback=mapsBaiduApiReady');
}

function mapsBaiduApiReady() {
	baiduIni();
}