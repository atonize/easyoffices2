'use strict';

var $ = window.jQuery;

//ON DOCUMENT READY
$(document).ready(function() {
    var Evento = function () {
        if (!Evento.instance) {
            this.events = {};
            Evento.instance = this;
        }
        return Evento.instance;
    };

    Evento.prototype = {
        on: function (eventName, callback) {
            this.events[eventName] = this.events[eventName] || [];
            this.events[eventName].push(callback)
        },

        emit: function (eventName, data) {
            if (this.events[eventName]) {
                this.events[eventName].forEach(function (callback) {
                    callback(data);
                })
            }
        }
    };

    // forEach function
    var forEach = function (array, callback, scope) {
        for (var i = 0; i < array.length; i++) {
            callback.call(scope, i, array[i]); // passes back stuff we need
        }
    };

    // tiny-slider initialisation
    var sliders = document.querySelectorAll('.slider');
    forEach(sliders, function (index, value) {
        var slider1 = tns({
            container: value,
            items: 1,
            slideBy: 'page',
            autoplay: false,
            mouseDrag: true,
            nav: false,
            controlsText: ['<svg role="img" class="icon icon-6-12"><use xlink:href="icons/icons.svg#arrow-left"></use></svg>','<svg role="img" class="icon icon-6-12"><use xlink:href="icons/icons.svg#arrow-right"></use></svg>'],
        });
    });

    // //Location slider
    if($('.location-slider').length) {
        var locationSlider = tns({
            container: '.location-slider',
            fixedWidth: 222,
            swipeAngle: false,
            speed: 400,
            gutter: 20,
            nav: false,
            controlsText: ['<img src="/images/icon-arrow-left.png">','<img src="/images/icon-arrow-right.png">'],
            mouseDrag: true,
            responsive: {
                0: {
                    fixedWidth: 150,
                    gutter: 10
                }, 
                500: {
                    fixedWidth: 240,
                    gutter: 20
                },
                1440: {
                    fixedWidth: 240
                }
              }
        });
    }

    if($('.posts-slider').length) {
        var postsSlider = tns({
            container: '.posts-slider',
            fixedWidth: 222,
            swipeAngle: false,
            speed: 400,
            gutter: 20,
            nav: false,
            controlsText: ['<img src="/images/icon-arrow-left.png">','<img src="/images/icon-arrow-right.png">'],
            mouseDrag: true,
            responsive: {
                0: {
                    fixedWidth: 150,
                    gutter: 10
                }, 
                500: {
                    fixedWidth: 240,
                    gutter: 20
                },
                1440: {
                    fixedWidth: 240
                }
              }
        });
    }

    //Customer reviews slider
    if($('.customer-reviews-slider').length) {
        var reviewsSlider = tns({
            container: '.customer-reviews-slider',
            items: 3,
            swipeAngle: false,
            speed: 400,
            gutter: 40,
            nav: false,
            mouseDrag: true,
            controlsText: ['<img src="/images/icon-left-large.png">','<img src="/images/icon-right-large.png">'],
            responsive: {
                0: {
                  items: 1,
                  controls: false
                },
                768: {
                    items: 2,
                    controls: true
                }, 
                1025: {
                    items: 3
                }
              }
        });
    }

    //Offices slider
    if($('.offices-slider').length) {
        var officeSlider = tns({
            container: '.offices-slider',
            items: 3,
            swipeAngle: false,
            speed: 400,
            gutter: 20,
            nav: false,
            mouseDrag: true,
            controlsText: ['<svg role="img" class="icon icon-6-12"><use xlink:href="icons/icons.svg#arrow-left"></use></svg>','<svg role="img" class="icon icon-6-12"><use xlink:href="icons/icons.svg#arrow-right"></use></svg>'],
            responsive: {
                0: {
                    controls: false,
                    items: 1,
                    fixedWidth: 300
                },
                768: {
                    items: 2,
                    fixedWidth: false,
                    center: true
                }, 
                1025: {
                    items: 3,
                    controls: true,
                    center: false
                }
              }
        });
    }

    //Customer reviews slider
    if($('#related-office').length) {
        var reviewsSlider = tns({
            container: '#related-office',
            items: 4,
            swipeAngle: false,
            speed: 400,
            gutter: 20,
            nav: false,
            lazyload: true,
            mouseDrag: true,
            controls: false,
            responsive: {
                0: {
                    fixedWidth: 150,
                    gutter: 10
                }, 
                500: {
                    fixedWidth: 240,
                    gutter: 20
                },
                1440: {
                    fixedWidth: 260
                }
              }
        });
    }

    //Get a quote form - datepicker field
    var userLang = 'navigator.language || navigator.userLanguage';  

    $( "#datepicker" ).datepicker({
        firstDay: 1,
        minDate: 1,
        beforeShow: function( input, inst){
            $(inst.dpDiv).addClass('datepicker');
        }
    });
    $.datepicker.setDefaults($.datepicker.regional[userLang]);
    $( "#datepicker" ).datepicker("setDate", new Date());

    $( "#slider-range-max" ).slider({
        range: "max",
        min: 1,
        max: 10,
        value: 2,
        slide: function( event, ui ) {
          $( "#amount" ).val( ui.value );
        }
      });
      $( "#amount" ).val( $( "#slider-range-max" ).slider( "value" ) );

    //Tabs
    var Tabs = function () {
        this.createTabIds();
        $(this.tabLink).on('click', this.onTabClick.bind(this));
    };

    Tabs.prototype = {
        tabLink: '.js-tab-link',
        tabContent: '.js-tab-content',

        createTabIds: function () {
            var allTabLinks = document.querySelectorAll(this.tabLink);
            var allTabContent = document.querySelectorAll(this.tabContent);
            allTabLinks.forEach(function (el, index) {
                el.setAttribute('data-tab', 'tab-' + index);
            });
            allTabContent.forEach(function (el, index) {
                el.setAttribute('id', 'tab-' + index);
            });
        },

        onTabClick: function (event) {
            this.selectedTabId = event.currentTarget.dataset.tab;
            this.showTab();
        },

        showTab: function () {
            var clickedTab = $("[data-tab='" + this.selectedTabId + "']")
            clickedTab.siblings().removeClass('current');
            clickedTab.parents('.js-tab-group').find(this.tabContent).removeClass('current');
            clickedTab.addClass('current');
            $('#' + this.selectedTabId).addClass('current');
        }
    };

    //Modal
    var Modal = function() {
        $('.js-modal-link').on('click', this.openModal.bind(this));
        $(document).on('click', this.closeModal.bind(this));
    };

    Modal.prototype = {

        openModal: function(e) {
            var modalId = e.currentTarget.dataset.id;
            $('#'+modalId).addClass('is-active');
        },
        
        closeModal: function(e) {
            var targetedModal = e.target;
            if (targetedModal.classList.contains('modal')) {
                targetedModal.classList.remove('is-active');
            } else if (targetedModal.classList.contains('js-modal-close')) {
                targetedModal.closest('.js-modal').classList.remove('is-active');
            }
        }
    };

    //show hide registraiton form
    var ShowHideRegistrationForm = function() {
        $('.js-open-register-form').on('click', this.showRegistrationForm.bind(this));
        $('.js-close-register-form').on('click', this.hideRegistrationForm.bind(this));
    }

    ShowHideRegistrationForm.prototype = {

        showRegistrationForm: function() {
            $('.js-register-workspace').addClass('is-active');
            $('html').css('overflow', 'hidden');
        },

        hideRegistrationForm: function() {
            $('.js-register-workspace').removeClass('is-active');
            $('html').css('overflow', 'visible');
        }
    };

    //Map toggle
    $('.js-toggle-map').on('click', function() {
        $(this).toggleClass('is-active');
        $('body').toggleClass('map-is-active');
    });

    //Mobile menu
    var Menu = function () {
        $('.js-toggle-menu').on('click', this.showHideMobileNav.bind(this));
        $('.js-toggle-menu-locations').on('click', this.checkIfPrimaryMenyIsOpened.bind(this));
        $('.js-toggle-search').on('click', this.showHideSearch.bind(this));
    };

    Menu.prototype = {

        closeSearchItemWhenClickOutside: function (e) {
            e.stopPropagation();
            if ($(e.target).parents('.js-search-item').length === 0) {
                $('.js-search-item').removeClass('is-active');
                $('body').removeClass('search-menu-active');
            }
        },

        showHideMobileNav: function () {
            $('html').toggleClass('menu-is-active');
        },

        showHideSearch: function() {
            $('body').toggleClass('search-is-active');
        },

        checkIfPrimaryMenyIsOpened: function() {
            if ($('html').hasClass('menu-is-active')) {
                this.toggleLocationsNav();
            }
        },

        toggleLocationsNav: function () {
            $('.header-nav-primary').toggleClass('is-hidden');
            $('.header-nav-secondary').toggleClass('is-active');
        }
    };

    //Filter module
    var Filters = function () {
        $('.js-filter-trigger').on('click', this.toggleFilterItem.bind(this));
        $('.js-close-filter').on('click', this.closeFilter.bind(this));
        $(document).on('click', this.closeFilterWhenClickOutside.bind(this));
    };

    Filters.prototype = {

        toggleFilterItem: function (e) {
            $('.js-filter').removeClass('is-active');
            var clickedLink = $(e.currentTarget);
            var clickedLinkParent = clickedLink.parent();
            clickedLinkParent.addClass('is-active');
            $('html').addClass('filter-active');
            this.hideMobileNav();
        },

        closeFilterWhenClickOutside: function (e) {
            e.stopPropagation();
            if ($(e.target).parents('.js-filter').length === 0) {
               this.hideFilter();
            }
        },

        hideFilter: function() {
            $('.js-filter').removeClass('is-active');
            $('html').removeClass('filter-active');
        },

        closeFilter: function() {
            this.hideFilter();
        },

        hideMobileNav: function () {
            $('html').removeClass('menu-is-active');
        }
    };

    //filter distance
    $(".filter-distance").each(function () {
        var $this = $(this);
        $(".filter-distance-slider", $this).slider({
            range: true,
            min: 1,
            max: 10,
            values: [1, 10],
            step: 0.1,
            slide: function (event, ui) {
                $(".filter-distance-data", $this).val(ui.values[0] + " - " + ui.values[1] + " miles");
            }
        });
        $(".filter-distance-data").val($(".filter-distance-slider").slider("values", 0) +
            " - " + $(".filter-distance-slider").slider("values", 1) + " miles");
    });

    //slider for number of peoples
    $( function() {
        $( "#number-of-people" ).slider({
          min: 1,
          max: 100,
          slide: function( event, ui ) {
            $( ".number-of-people-value" ).attr('data-value', ui.value );
          }
        });
        $( ".number-of-people-value" ).attr('data-value', $( "#number-of-people" ).slider( "value" ) );
      } );

    // Smooth scrolling
    $('a[href^="#"]').on('click', function(event) {

        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 100
            });
        }
    });

    var ShortlistControler = function () {
        this.createStorageObject();

        $('.js-add-to-shortlist').on('click', this.tryAddItem.bind(this));
        $('body').on('click', '.js-shortlist-overlay-item-remove', this.tryRemoveItem.bind(this));
        this.numberOfItems = JSON.parse(localStorage.getItem('shortlist')).length == 0
                ? 0
                : JSON.parse(localStorage.getItem('shortlist')).length;
    };

    ShortlistControler.prototype = {
        maxNumberOfItems: 6,

        createStorageObject: function() {
            if (localStorage.getItem('shortlist') == null) {
                localStorage.setItem('shortlist', JSON.stringify([]));
            }
        },

        tryAddItem: function (e) {
            if (this.numberOfItems < this.maxNumberOfItems) {
                this.addToList(e);
            } else {
                alert('Shortlist reached maximum number of items!');
            }
        },

        tryRemoveItem: function(e) {
            if (this.numberOfItems !== 0) {
                this.removeFromList(e);
            } else {
                alert('Shortlist is empty!');
            }
        },

        addToList: function (e) {
            this.numberOfItems++;
            new Evento().emit('SHORTLIST|AMOUNT|INCREMENT', { 
                    name: e.currentTarget.dataset.name, 
                    image: e.currentTarget.dataset.image, 
                    price: e.currentTarget.dataset.price, 
                    propertyId: e.currentTarget.dataset.id 
                });
        },

        removeFromList: function(e) {

            this.numberOfItems--;
            new Evento().emit('SHORTLIST|AMOUNT|DECREMENT', { 
                numberOfItems: this.numberOfItems,
                propertyId: e.currentTarget.dataset.id 
            });
        }
    };

    var ShortlistView = function () {
        this.allListItems = JSON.parse(localStorage.getItem('shortlist'));
        this.numberOfItems = this.allListItems.length == 0 
            ? 0
            : this.allListItems.length;

        if (this.allListItems.length != 0) {
            this.onPageChangeUpdates();
        }

        new Evento().on('SHORTLIST|AMOUNT|INCREMENT', this.addItemToTheList.bind(this));
        new Evento().on('SHORTLIST|AMOUNT|DECREMENT', this.removeItemFromTheList.bind(this));

        $('.js-shortlist-number-of-items-link').on('click', this.toggleShortlistOverlay.bind(this));
        $('.js-shortlist-overlay-close').on('click', this.hideShortlistOverlay.bind(this));
        // $('body').on('click', this.hideShortlistOverlayWhenClickOutside.bind(this));
    };

    ShortlistView.prototype = {
        allListItems: [],
        maxNumberOfItems: 6,

        onPageChangeUpdates: function() {
            this.createMarkupForItem();
            this.updateGlobalAmount();
            this.hideEmptyContent();
            this.markTotalCounter();
            this.indicateAllFavorites();
        },

        toggleShortlistOverlay: function() {
            $('.js-shortlist-overlay').toggleClass('is-active');
        },

        hideShortlistOverlay: function() {
            $('.js-shortlist-overlay').removeClass('is-active');
        },

        showShortlistOverlay: function() {
            $('.js-shortlist-overlay').addClass('is-active');
        },

        hideShortlistOverlayWhenClickOutside: function(event) {
            event.stopPropagation();
            
            if (!$(event.target).closest('.js-shortlist-overlay, .js-shortlist-number-of-items-link, .js-add-to-shortlist, .js-shortlist-overlay-item-remove').length) {
                this.hideShortlistOverlay();
              }
        },

        updateAmount: function() {
            this.updateGlobalAmount();
            this.indicateAmountState();
        },

        updateGlobalAmount: function() {
            $('.js-shortlist-number-of-items').text(this.numberOfItems);
        },
        
        indicateAmountState: function() {
            this.numberOfItems > 0 
                ? this.amountIsNotEmpty()
                : this.amountIsEmpty();
        },

        amountIsEmpty: function() {
            this.unmarkTotalCounter();
            this.showEmptyContent();
            this.hideShortlistOverlay();
        },

        amountIsNotEmpty: function() {
            this.markTotalCounter();
            this.hideEmptyContent();
            this.showShortlistOverlay(); 
        },

        markTotalCounter: function() {
            $('.js-shortlist-number-of-items-link').addClass('is-not-empty');
        },

        unmarkTotalCounter: function() {
            $('.js-shortlist-number-of-items-link').removeClass('is-not-empty');
        },

        showEmptyContent: function() {
            $('.js-shortlist-overlay').addClass('is-empty');
            $('.js-shortlist').addClass('is-empty');
        },

        hideEmptyContent: function() {
            $('.js-shortlist-overlay').removeClass('is-empty');
            $('.js-shortlist').removeClass('is-empty');
        },

        addItemToTheList: function (data) {
            this.allListItems.push(data);
            this.numberOfItems = this.allListItems.length;
            this.updateStorageValue();
            this.updateAmount(data);
            this.indicateFavoriteIsAdded(data.propertyId);
            this.createMarkupForItem();
        },

        removeItemFromTheList: function(data) {
            var itemForRemoveId = data.propertyId;
            var itemForRemoveIndex;

            this.allListItems.forEach(function (item, index) {
                if (item.propertyId === itemForRemoveId) {
                    return itemForRemoveIndex = index;
                }
            });
            this.allListItems.splice(itemForRemoveIndex, 1);
            this.numberOfItems = data.numberOfItems;
            this.updateStorageValue();
            this.updateAmount();
            this.indicateFavoriteIsRemoved(itemForRemoveId);
            this.createMarkupForItem();
        },

        updateStorageValue: function() {
            localStorage.setItem('shortlist', JSON.stringify(this.allListItems));
        },

        indicateAllFavorites: function() {
            var self = this;
            this.allListItems.forEach(function(item) {
                self.indicateFavoriteIsAdded(item.propertyId);
            });
        },

        indicateFavoriteIsRemoved: function(propertyId) {
            $(".js-shortlist-overlay-item-remove[data-id='" + propertyId + "']").addClass('hide').siblings().removeClass('hide');
        },

        indicateFavoriteIsAdded: function(propertyId) {
            $(".js-add-to-shortlist[data-id='" + propertyId + "']").addClass("hide").siblings().removeClass('hide');
        },

        createMarkupForItem: function() {
            var itemsMarkup = '';
            this.allListItems.forEach(function(item) {
                itemsMarkup +=
                    '<div class="shortlist-overlay-item">' +
                        '<a href="javascript:;" class="shortlist-overlay-item-link">' +
                            '<img src="' + item.image + '" alt="' + item.name + '">' +
                            '<div class="shortlist-overlay-item-info">' +
                                '<h4 class="shortlist-overlay-item-info-place">' + item.name + '</h4>' +
                                '<p class="shortlist-overlay-item-info-price">' + item.price + '</p>' +
                                '<p class="shortlist-overlay-item-info-view">View space</p>' +
                            '</div>' +
                        '</a>' +
                        '<a href="javascript:;" class="js-shortlist-overlay-item-remove shortlist-overlay-item-remove" data-id="' + item.propertyId + '">' +
                            '<svg role="img" class="icon icon--20-20">' +
                                '<use xlink:href="icons/icons.svg#icon-trash"></use>' +
                            '</svg>' +
                        '</a>' +
                    '</div>';
            });
            $('.js-shorlist-items').html(itemsMarkup);
        }
    };

    new Menu();
    new Tabs();
    new Modal();
    new ShowHideRegistrationForm();
    new Filters();

    new ShortlistControler();
    new ShortlistView();    

});


//WINDOW ONLOAD
$(window).on("load",function() {
    
    var IsotopeFilter = function() {
        if ($('.js-location-filter').length) {
            this.container = $('.js-location-filter').isotope();
            $('.js-location-filter').isotope(); //solve problem with initial portfolio height with responsive elements
        }
        
        $('.js-location-filter-nav-item').on('click', this.filterItemsOnNavClick.bind(this));
        this.activateFirstItemInNav();
        this.updateFilterCounts();
    };

    IsotopeFilter.prototype = {

        activateFirstItemInNav: function() {
            $('.js-location-filter-nav-item:first').addClass('is-active');
        },

        filterItemsOnNavClick: function(e) {
            var clickedLink = $(e.currentTarget);;
            var filterValue = $(clickedLink).attr('data-filter');
            this.container.isotope({ filter: filterValue });
            clickedLink.addClass('is-active').siblings().removeClass('is-active');
        },

        updateFilterCounts: function() {
            var allFilterLinks = $('.js-location-filter-nav-item');
            allFilterLinks.each(function(i, item) {
                var singleFilterLink = $(item);
                var filterValue = singleFilterLink.attr('data-filter');
                if (!filterValue) {
                    return;
                }
                var count = $(filterValue + ' .location-filter-item-desc-link').length;
                singleFilterLink.find('.filter-count').text(count);
            });
        }
    };

    new IsotopeFilter();
});

var iconNormal = 'images/pin1.png';
var iconSelected = 'images/pin.svg';  
var iconLarge = 'images/icon-map-large.svg';

//Map with multiple properties
var MapWithProperties = function() {
    if($('#map').length) {
        this.getPropertiesDataFromMarkup();
        this.initMap();
    }
}

MapWithProperties.prototype = {
    map: null,
    markers: [],
    properties: [],
    propertiesData: $(".js-property-item"),
    
    getPropertiesDataFromMarkup: function () {
        for (var i = 0; i < this.propertiesData.length; i++) {
            var lat = this.propertiesData[i].dataset.lat;
            var lng = this.propertiesData[i].dataset.lng;
            var name = this.propertiesData[i].dataset.name;
            var image = this.propertiesData[i].dataset.image;
            var propertyId = this.propertiesData[i].dataset.propertyId;
            this.properties.push([
                '<div class="map-infobox">' +
                '<img src="' + image + '" class="map-infobox-image" />' +
                '<h5 class="map-infobox-title">' + name + '</h5>' +
                '<a href="' + propertyId + '" class="map-infobox-link">View space</a>'+
                '</div>'
                ,lat, lng
            ]
            );
        }
    },
    
    initMap: function () {
        this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            styles: [
                {
                    featureType: "all",
                    stylers: [
                        { saturation: -80 },
                        { lightness: 50 }
                    ]
                }
            ],
            fullscreenControl: false,
            streetViewControl: false,
            mapTypeControl: false,
            zoomControlOptions: {
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            }
        });
        this.setMarkers(this.map);
    },
    
    setMarkers: function (map) {
        var bounds = new google.maps.LatLngBounds();
        var markers = [];
        
        for (var i = 0; i < this.properties.length; i++) {
            var property = this.properties[i];
            var marker = new google.maps.Marker({
                position: {
                    lat: parseFloat(property[1]),
                    lng: parseFloat(property[2])
                },
                map: this.map,
                animation: google.maps.Animation.DROP,
                title: property[0],
                icon: iconNormal
            });
            bounds.extend(marker.getPosition());
            
            // set infobox for all markers
            var infowindow = new google.maps.InfoWindow({
                content: property[0]
            });
            
            marker.infobox = infowindow;
            markers.push(marker);
            
            // open marker on click
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    // Close all other infoboxes
                    for (var j = 0; j < markers.length; j++) {
                        markers[j].infobox.close(map);
                        markers[j].setIcon(iconNormal);
                    }
                    // Open correct info box
                    markers[i].infobox.open(map, markers[i]);
                    markers[i].setIcon(iconSelected);
                }
            })(marker, i));
            
            //Close infowindow when click outside
            google.maps.event.addListener(map, "click", function (event) {
                markers.forEach(function (item) {
                    item.infobox.close(map);
                    item.setIcon(iconNormal);
                });
            });
        }
        
        this.map.fitBounds(bounds);
        this.markers = markers;
    }
};

new MapWithProperties();

//Map multiple properties - one active on load
var SingleMap = function() {
    this.initMap();
    this.writeAddress();
    this.getCurrentLocation();
    $('.js-get-directions').on('click', this.getDirections.bind(this));
}

SingleMap.prototype = {
    mapEl: document.getElementById("map-single"),
    mapInstance: {},
    directionsDisplay: new google.maps.DirectionsRenderer,
    directionsService: new google.maps.DirectionsService,
    // currentPosition: {
    //     lat: 51.432190,
    //     lng: -0.525320
    // },
    address: $('#map-single').data('address'),
    currentPosition: {},

    initMap: function() {
        this.lat = this.mapEl.getAttribute('data-lat');
        this.lng = this.mapEl.getAttribute('data-lng');
        this.mapInstance = new google.maps.Map(this.mapEl, {
            zoom: 17,
            center: new google.maps.LatLng(parseFloat(this.lat), parseFloat(this.lng)),
            mapTypeId: 'roadmap',
            styles: [
                {
                    featureType: "all",
                    stylers: [
                        { saturation: -80 },
                        { lightness: 50 }
                    ]
                }
            ],
            fullscreenControl: false,
            streetViewControl: false,
            mapTypeControl: false,
            zoomControlOptions: {
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            }
        });

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(parseFloat(this.lat), parseFloat(this.lng)),
            map: this.mapInstance,
            icon: iconLarge
        });
    },

    writeAddress: function() {
        $('.js-map-office-address').text(this.address);
    },

    getCurrentLocation: function() {
        var self = this;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                self.currentPosition = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
            }, function () {
                console.log('cannot get location');
            });
        } else {
            window.alert('Geolocation is not supported');
        }
    },

    getDirections: function() {
        this.directionsDisplay.setMap(this.mapInstance);
        this.calculateAndDisplayRoute(this.directionsService, this.directionsDisplay, this.currentPosition, this.lat, this.lng);
    },

    calculateAndDisplayRoute: function(directionsService, directionsDisplay, currentPos, latDes, lngDes) {
        var latDest = parseFloat(latDes);
        var lngDest = parseFloat(lngDes);
        var dest = { lat: JSON.parse(latDest), lng: JSON.parse(lngDest) };

        directionsService.route({
            origin: currentPos,  // current position.
            destination: dest,  // destination.
            travelMode: google.maps.TravelMode.DRIVING
        }, function (response, status) {
            if (status == 'OK') {
                directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }
}

if($('#map-single').length) {
    new SingleMap();
}

function autoGrow(element) {
    element.style.height = (element.scrollHeight)+"px";

    if (element.value == "") {
        element.removeAttribute("style");
    }
}